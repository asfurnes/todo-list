package fxutil.doc;

public interface DocumentPersistence<D, L> extends DocumentLoader<D>, DocumentSaver<D, L> {
}
